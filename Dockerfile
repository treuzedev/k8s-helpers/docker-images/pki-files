FROM amazon/aws-lambda-provided:al2 as tmp

WORKDIR /var/task
COPY app/bootstrap /var/runtime
COPY app/function.sh /var/task
COPY app/ca-config.json /var/runtime

RUN yum upgrade -y && \
    yum install -y jq unzip && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    aws --version && \
    rm -rf awscliv2.zip aws && \
    curl https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssl --output cfssl --silent && \
    curl https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssljson --output cfssljson --silent && \
    yum remove -y unzip && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    chmod +x cfssl cfssljson && \
    mv cfssl cfssljson /usr/local/bin/ && \
    chmod 755 /var/runtime/bootstrap /var/task/function.sh
    
FROM tmp

CMD ["function.sh.handler"]
