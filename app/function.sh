function handler () {
  
  # cd to tmp folder
  # only place where files are allowed to be created
  echo "- changing workdir to tmp" 1>&2
  cd /tmp
  
  echo "- make sure environment is empty" 1>&2
  rm -rf *

  # get the data
  echo "- getting the data" 1>&2
  EVENT_DATA=$1

  # log the event to stderr
  echo "- logging payload to stderr" 1>&2
  echo "$EVENT_DATA" 1>&2;
  
  # extract variables
  echo "- extracting data from payload" 1>&2
  O=$(echo $EVENT_DATA | jq -r '.cert.O')
  C=$(echo $EVENT_DATA | jq -r '.cert.C')
  L=$(echo $EVENT_DATA | jq -r '.cert.L')
  BUCKET=$(echo $EVENT_DATA | jq -r '.bucket')
  PKI_FILES_DIR=$(echo $EVENT_DATA | jq -r '.pki_files_dir')
  LB_PUBLIC_IP=$(echo $EVENT_DATA | jq -r '.lb_public_ip')
  NODE_0_IP=$(echo $EVENT_DATA | jq -r '.node_0_ip')
  NODE_1_IP=$(echo $EVENT_DATA | jq -r '.node_1_ip')
  NODE_2_IP=$(echo $EVENT_DATA | jq -r '.node_2_ip')
  K8S_HOSTNAMES=s"kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local"
  K8S_APISERVER_HOSTNAMES="127.0.0.1,$LB_PUBLIC_IP,10.32.0.1,$NODE_0_IP,$NODE_1_IP,$NODE_2_IP,$K8S_HOSTNAMES"
  
  # make sure payload had the necessary variables
  echo "- checking if all necessary variables are valid" 1>&2
  case 'null' in
    $O) exit 1 ;;
    $C) exit 1 ;;
    $L) exit 1 ;;
    $BUCKET) exit 1 ;;
    $LB_PUBLIC_IP) exit 1 ;;
    $NODE_0_IP) exit 1 ;;
    $NODE_1_IP) exit 1 ;;
    $NODE_2_IP) exit 1 ;;
  esac
  
  # create necessary files
  echo "- generating necessary files.."
  echo "creating ca-csr.json" 1>&2
  echo "{
    \"CN\": \"$O\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"$O\"
      }
    ]
  }" > ca-csr.json
  
  echo "creating admin-csr.json" 1>&2
  echo "{
    \"CN\": \"admin\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"system:masters\"
      }
    ]
  }" > admin-csr.json
  
  echo "creating kubernetes-csr.json"
  echo "{
    \"CN\": \"kubernetes\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"Kubernetes\"
      }
    ]
  }" > kubernetes-csr.json
  
  echo "creating service-account-csr.json"
  echo "{
    \"CN\": \"service-accounts\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"Kubernetes\"
      }
    ]
  }" > service-account-csr.json
  
  echo "creating kube-controller-manager-csr.json"
  echo "{
    \"CN\": \"system:kube-controller-manager\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"system:kube-controller-manager\"
      }
    ]
  }" > kube-controller-manager-csr.json
  
  echo "creating kube-proxy-csr.json" 1>&2
  echo "{
    \"CN\": \"system:kube-proxy\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"system:node-proxier\"
      }
    ]
  }" > kube-proxy-csr.json
  
  echo "creating kube-scheduler-csr.json" 1>&2
  echo "{
    \"CN\": \"system:kube-scheduler\",
    \"key\": {
      \"algo\": \"rsa\",
      \"size\": 2048
    },
    \"names\": [
      {
        \"C\": \"$C\",
        \"L\": \"$L\",
        \"O\": \"system:kube-scheduler\"
      }
    ]
  }" > kube-scheduler-csr.json
  
  echo "- generating ca cert and private key" 1>&2
  cfssl gencert -initca ca-csr.json | cfssljson -bare ca
  
  echo "- generating admin user cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    admin-csr.json | cfssljson -bare admin
    
  echo "- generating kubernetes api server cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    -hostname=$K8S_APISERVER_HOSTNAMES \
    kubernetes-csr.json | cfssljson -bare kubernetes
    
  echo "- generating service accounts server cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    service-account-csr.json | cfssljson -bare service-account
    
  echo "- generating the kube controller manager cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager
    
  echo "- generating kube-proxy user cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    kube-proxy-csr.json | cfssljson -bare kube-proxy
    
  echo "- generating kube-scheduler user cert and private key" 1>&2
  cfssl gencert \
    -ca=ca.pem \
    -ca-key=ca-key.pem \
    -config=/var/runtime/ca-config.json \
    -profile=kubernetes \
    kube-scheduler-csr.json | cfssljson -bare kube-scheduler
    
  # move files to a dir for s3 upload
  echo "- moving files to dir for s3 upload" 1>&2
  mkdir -p /tmp/files
  mv \
    ca-csr.json ca-key.pem ca.csr ca.pem \
    admin-csr.json admin-key.pem admin.csr admin.pem \
    kubernetes-csr.json kubernetes.pem kubernetes-key.pem kubernetes.csr \
    service-account-csr.json service-account.pem service-account-key.pem service-account.csr \
    kube-controller-manager-csr.json kube-controller-manager.pem kube-controller-manager-key.pem kube-controller-manager.csr \
    kube-proxy-csr.json kube-proxy-key.pem kube-proxy.csr kube-proxy.pem \
    kube-scheduler-csr.json kube-scheduler-key.pem kube-scheduler.csr kube-scheduler.pem \
    /tmp/files
  cp /var/runtime/ca-config.json /tmp/files
    
  # upload files to s3 bucket
  echo "- uploading files to s3" 1>&2
  aws s3 cp /tmp/files s3://$BUCKET/$PKI_FILES_DIR --recursive
    
  # remove files after s3 upload
  echo "- cleanup environment" 1>&2
  rm -rf *

  # reply to lambda service an okay message
  echo "- returning an answer to lambda" 1>&2
  RESPONSE="PKI creation successful!"
  echo $RESPONSE
  
}
